module.exports = function (grunt) {
  grunt.initConfig({
    bower_concat: {
      build: {
        options: {
          separator: ";"
        },
        dest: "www/scripts/libs.js",
        cssDest: "www/css/style.css",
        mainFiles: {
          "jquery-validation": ["jquery.validate.js"],
          "jquery-validation-unobtrusive": ["jquery.validate.unobtrusive.js"],
          "jquery-ajax-unobtrusive": ["jquery.unobtrusive-ajax.js"],
          "bootstrap": ["dist/css/bootstrap.css", "dist/js/bootstrap.js"]
        }
      }
    },
    uglify: {
      build: {
        files: {
          "www/scripts/libs.min.js": ["www/scripts/libs.js"]
        }
      }
    },
    cssmin: {
      build: {
        files: {
          "www/css/style.min.css": ["www/css/style.css"]
        }
      }
    },
    copy: {
      build: {
        expand: true,
        flatten: true,
        src: "bower_components/bootstrap/dist/fonts/*",
        dest: "www/fonts/"
      }
    }
  });

  grunt.loadNpmTasks("grunt-bower-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-copy");

  grunt.registerTask("default", ["bower_concat", "uglify", "cssmin", "copy"]);
};