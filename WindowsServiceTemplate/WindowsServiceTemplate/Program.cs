﻿using System;
using System.ServiceProcess;
using System.Text;
using WindowsServiceTemplate.Helpers;
using CommandLine;
using log4net;

namespace WindowsServiceTemplate
{
    class Program
    {
        internal static readonly ILog Logger = LogManager.GetLogger("ServiceLogger");

        static void Main(string[] args)
        {
            var options = new Options();

            var parser = new Parser(conf =>
            {
                conf.MutuallyExclusive = true;
                conf.CaseSensitive = false;
                conf.IgnoreUnknownArguments = true;
                conf.HelpWriter = Console.Error;
            });

            if (parser.ParseArguments(args, options))
            {
                //Install as service
                if (options.Install)
                {
                    Console.WriteLine(InstallHelper.Install()
                        ? @"Service installed."
                        : @"Failed to install service.");
                    return;
                }

                //Uninstall service
                if (options.Uninstall)
                {
                    Console.WriteLine(InstallHelper.Uninstall()
                        ? @"Service uninstalled."
                        : @"Failed to uninstall service.");
                    return;
                }

                //Start service as console app
                var service = new DefaultService();
                var servicesToRun = new ServiceBase[] { service };

                if (Environment.UserInteractive)
                {
                    Console.OutputEncoding = Encoding.UTF8;

                    Console.CancelKeyPress += (x, y) => service.Stop();
                    service.Start();
                    Console.WriteLine(@"Service is running, press a key to stop.");
                    Console.ReadKey();
                    service.Stop();
                    Console.WriteLine(@"Service stopped.");
                }
                else
                {
                    ServiceBase.Run(servicesToRun);
                }
            }
            else
            {
                Environment.ExitCode = 1;
            }
        }
    }
}