﻿using System;
using Quartz;

namespace WindowsServiceTemplate.Jobs
{
    class DefaultJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                //Do some stuff here
                Console.WriteLine("I am working...");

                throw new Exception("Hello, log4net!");
            }
            catch (Exception ex)
            {
                Program.Logger.Error("DefaultJob error", ex);
            }
        }
    }
}