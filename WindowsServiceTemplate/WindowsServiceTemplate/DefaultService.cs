﻿using System.ServiceProcess;
using WindowsServiceTemplate.Jobs;
using Quartz;
using Quartz.Impl;

namespace WindowsServiceTemplate
{
    partial class DefaultService : ServiceBase
    {
        private readonly IScheduler _scheduler;

        public DefaultService()
        {
            InitializeComponent();

            _scheduler = new StdSchedulerFactory().GetScheduler();

            //Configure your jobs shedule here
            var job = JobBuilder.Create<DefaultJob>()
                .WithIdentity("job", "group1")
                .Build();

            var trigger = TriggerBuilder.Create()
              .WithIdentity("trigger", "group1")
              .WithSimpleSchedule(x => x.WithIntervalInSeconds(5).RepeatForever())
              //.WithCronSchedule("0 0 9-18 ? * MON,TUE,WED,THU,FRI")
              .ForJob(job)
              .Build();

            _scheduler.ScheduleJob(job, trigger);
        }

        public void Start()
        {
            _scheduler.Start();
        }

        public new void Stop()
        {
            _scheduler.Shutdown();
        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        protected override void OnStop()
        {
            Stop();
        }
    }
}